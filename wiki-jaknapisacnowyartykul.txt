<div>

Przede wszystkim jeśli dopiero zaczynasz przygodę z Wikipedią, to lepiej nie zaczynaj jej od tworzenia nowych artykułów. Naucz się najpierw podstaw edytowania i formatowania. Jeśli jednak czujesz się na siłach, to zapraszamy!

<span style="font-weight:bold;">I. Encyklopedyczność i źródła</span>
: Zastanów się albo spytaj na [[wikipedia:Discord|naszym Discordzie]], czy Twój temat jest [[Wikipedia:Encyklopedyczność|encyklopedyczny]]. Na Wikipedii piszemy o bardzo wielu rzeczach, ale nie o wszystkim.
:Musisz mieć też pod ręką rzetelne [[wikipedia:Weryfikowalność|źródła]], dla swojego tematu. Nie możesz pisać informacji, których nigdzie nie opublikowano. [[Pomoc:Jak unikać podejrzeń o NPA|Powtarzaj informacje, ale nie kopiuj ich dosłownie]]. Po każdej informacji podaj [[Pomoc:Przypisy|przypis]], tak aby inni mogli zweryfikować podane fakty.

</div>
<div>
<span style="font-weight:bold;">II. Nazwa i forma artykułu</span>
: Wiele tematów można opisać pod różnymi nazwami. Jeśli nie wiesz, która nazwa jest prawidłowa lub właściwsza, to sprawdź, jakiej nazwy używają Twoje najbardziej wiarygodne źródła. Możesz też zobaczyć, jaki tytuł ma odpowiedni artykuł w innych encyklopediach (np: [http://encyklopedia.pwn.pl/ PWN]). Więcej wskazówek znajdziesz na stronie: [[Wikipedia:Nazewnictwo artykułów]].
: Artykuł powinien zaczynać się od [[Pomoc:Jak napisać dobrą definicję|zrozumiałej, krótkiej definicji]]. Pamiętaj, że piszemy tu encyklopedię, w której obowiązuje zasada [[wikipedia:Neutralny punkt widzenia|neutralnego punktu widzenia]]. Artykuł ma się ograniczać do podania wiarygodnych faktów dotyczących bezpośrednio jego tematu. Nie jest to miejsce na przedstawianie własnych teorii, opinii, poglądów i odczuć. Należy też unikać wyrażeń [[Wikipedia:Unikaj pustosłowia|pustych]] i [[Wikipedia:Unikaj wyrażeń zwodniczych|zwodniczych]].
:Więcej wskazówek znajdziesz na stronie: [[Pomoc:Styl – poradnik dla autorów]].
</div>

<div>
<span style="font-weight:bold;">III. Utwórz nową stronę, na której chcesz pisać</span>

: Zacznij od utworzenia strony w formie swojego [[wikipedia:Strona_użytkownika#Brudnopisy_i_podstrony|brudnopisu-podstrony]]:
: <inputbox>
type=create
placeholder=Wpisz nazwę artykułu
prefix=Special:Mypage/
buttonlabel=Rozpocznij pisanie
width=30
</inputbox>

: Po ukończeniu prac nad treścią, [[Pomoc:Zmiana nazwy strony|przenieś]] artykuł pod odpowiednią nazwę. Osoby początkujące powinny poprosić kogoś o sprawdzenie artykułu.
:''Alternatywnie'' możesz wpisać do wyszukiwarki Wikipedii nazwę artykułu. W wynikach wyszukiwania wyświetli się informacja w rodzaju „Strony »Abc« nie ma w Wikipedii. Możesz ją ''utworzyć''… ”. ''Czerwony link'' prowadzi do edycji nowego artykułu.
</div>

<div>
<span style="font-weight:bold;">IV. Podpatrz dobre wzorce</span>
: Zobacz co dziś prezentujemy w [[Wikiprojekt:Czy wiesz/ekspozycje/{{#timel:Y-m-d}}|rubryce „Czy wiesz”]]. Możesz wzorować się na tych, a także na [[Wikipedia:Dobre Artykuły|dobrych]] i [[Wikipedia:Artykuły na Medal|medalowych artykułach]].
</div>

<div>
<span style="font-weight:bold;">V. Jeżeli chcesz pisać etapami</span>
: Wstaw na początku artykułu szablon {{s|W edycji}}, by pokazać innym, że pracujesz nad danym artykułem. Usuń go jednak niezwłocznie po wykonaniu wszystkich edycji.
</div>

== Propozycje artykułów do napisania ==
; Teksty obcojęzyczne: Nie publikuj w polskojęzycznej Wikipedii tekstów obcojęzycznych. Odpowiednim miejscem dla nich są inne wersje językowe Wikipedii.
; Tłumaczenia: Możesz tworzyć artykuły z obcojęzycznych wersji Wikipedii. Tłumacz tylko [[Wikipedia:Weryfikowalność|weryfikowalne]] artykuły, podając oryginalne źródła, w miarę możliwości sprawdzając pochodzące z nich informacje. Na stronie [[Wikipedia:Propozycje tematów/Najwięcej interwiki|Propozycje tematów/Najwięcej interwiki]] znajdują się artykuły, które istnieją w innych Wikipediach poza polskojęzyczną (zwróć jednak uwagę na to, czy zawierają źródła).
{{osobny artykuł|Wikipedia:Tłumaczenia}}
; Szukaj tematu: Przeglądaj artykuły z interesującego cię działu. Wcześniej czy później trafisz na czerwone odsyłacze, wskazujące na artykuły nieobecne w Wikipedii. Mamy też wykaz [[Wikipedia:Propozycje tematów|propozycji tematów]]. Zobacz też kategorię [[:Kategoria:Propozycje tematów|Propozycje tematów]] oraz projekt [[Wikiprojekt:Tygodnie tematyczne|Tygodnie tematyczne]].
</div>
[[Kategoria:Pomoc - edytowanie stron]]
